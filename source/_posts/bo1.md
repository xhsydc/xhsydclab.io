---
title: 使用next主题美化博客站点
tags: [博客]
---
### 						使用next主题美化博客站点
<!-- more -->
上一篇我们介绍了怎么搭建一个个人博客站点，这次我们来美化一下站点

#### 1.准备阶段

​	a.我们需要准备一个github账号，https://github.com/next-theme/hexo-theme-next

​	b.注册账号后需要生成秘钥，然后绑定本地机器。具体操作百度，这里不详细说明

​	c.把GitHub上next主题拉取到我们的站点中

```shell
-- 首先cd进你得站点总目录
cd <folder>
git clone https://github.com/next-theme/hexo-theme-next themes/next
```

​	d.然后配置<folder>/_config.yml文件

```yaml
theme: next
```

​	e.本地预览结果，一种黑白风的博客网站，目前基础的美化已经完成。下面我们做一些特殊的美化功能

```shell
--本地预览执行命令
hexo d -g
hexo server
```

![image-20210824153323032](image-20210824153323032.png)

#### 2.配置阶段

​	a.我们首先配置评论功能

​		我们使用utterances插件来实现github账号评论功能，

​		1.首先我们在GitHub上新建一个空项目，作为评论后台。确保目录settings/options/issues功能是勾选打开的状态

![image-20210825143604988](1.png)

​	2.在你得GitHub上下载https://github.com/apps/utterances app

​	3.站点配置使用utterances功能

```shell
comments:
 #修改使用utterances
  active: utterances

# Utterances
# For more information: https://utteranc.es
utterances:
  enable: true
  repo: xhsydc/ydcgitcoment # 使用上面新增的作为评论的项目，
  # Available values: pathname | url | title | og:title
  issue_term: pathname
  # Available values: github-light | github-dark | preferred-color-scheme | github-dark-orange | icy-dark | dark-blue | photon-dark | boxy-light
  theme: github-light
```

​	4.发布运行，查看效果

![image-20210825144407123](2.png)

