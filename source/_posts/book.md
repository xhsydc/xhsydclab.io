---
title: 从零开始搭建一个自己的博客
tags: [博客]
---
### 									从零开始搭建一个自己的博客

##### 1.配置环境

​		a.安装[nodejs]() http://nodejs.cn/download/包

​		b.安装[git]() https://git-scm.com/download/win

​		c.安装hexo，我使用的是window系统，上面两个安装完成后，直接cmd打开
<!-- more -->
```shell
npm install -g hexo-cli
npm install hexo
即可安装完成hexo
```

​		安装完成后，会在你打开的cmd目录下生成一个node_modules 文件夹，下面我们配置环境变量，让hexo命令可全局生效

​		d.配置环境变量 C:\Users\Administrator\node_modules\.bin （这是我的全路径，需要改成你们自己的），添加到path环境变量中

​		hexo和nodejs对应的版本兼容性，要注意下载的版本是否支持。查看版本，直接cmd中输入hexo -v就可以

| Hexo 版本   | 最低兼容 Node.js 版本 |
| :---------- | :-------------------- |
| 5.0+        | 10.13.0               |
| 4.1 - 4.2   | 8.10                  |
| 4.0         | 8.6                   |
| 3.3 - 3.9   | 6.9                   |
| 3.2 - 3.3   | 0.12                  |
| 3.0 - 3.1   | 0.10 or iojs          |
| 0.0.1 - 2.8 | 0.10                  |

##### 2.搭建本地网站

​	a.上述环境全部准备好之后，初始化一个本地站点

```shell
hexo init <folder>
cd <folder>
npm install
-- <folder>是你本地的站点总目录
```

folder文件夹说明

```shell
.
├── _config.yml --配置文件，修改站点各种属性的地方
├── package.json --三方依赖的应用程序
├── scaffolds --博客的模板文件
├── source --博客文章的总目录
|   ├── _drafts --草稿
|   └── _posts --正文
└── themes --主题
```

​	b.上述操作初始化完成后，可以直接阅览本地的博客网站样式

```shell
hexo g -d
hexo server
```

​	打开http://localhost:4000/ 可以看到下图的样式，说明本地博客已经搭建完成了。

![image-20210823164229071](image-20210823164229071.png)

##### 3.发布本地站点去公网

​	我们本次发布选择[https://gitlab.com/dashboard/projects]() gitlab网站发布自己的博客，当然部署的服务器有很多种，可以自己选择

​	a. 注册一个账号，新建一个以`<你的 GitLab 用户名>.gitlab.io`命名的项目。

​	b.然后进入你本地的博客目录，

​		cd  <folder>

​	新建一个CICD的脚本名称为  **.gitlab-ci.yml**

```shell
image: node:10-alpine # use nodejs v10 LTS
cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - npm install

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
      - master
```

​	上面操作完成后，上传你本地的站点至公网上

```shell
$ git init
$ git remote add origin git@gitlab.com:<你的 GitLab 用户名>/<你的 GitLab 用户名>.gitlab.io.git
$ git add .
$ git commit -m "init blog"
$ git push -u origin master
```

​	c.当你完成上面步骤时，就可以访问了

​	https://<你的 GitLab 用户名>.gitlab.io

![image-20210823182744386](image-20210823182744386.png)

​	d.以上就是我们整个搭建过程

##### 4.可能遇到的问题

​	a.发布至公网时，还是访问不了。这时候你需要在项目页面访问**设置->General->Visibility, project features, permissions** 权限设置为public，就可以了。

