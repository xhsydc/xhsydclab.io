---
title: 美化博客站点3-全局搜索和样式
tags: [博客]
---
### 					美化博客站点3-全局搜索和样式
<!-- more -->
**配置文件目录没有特殊说明，都是主题文件夹下/next/_config.yml文件**

###### 1.全局搜索

a.配置如下

```shell
--第一步
npm install hexo-generator-searchdb
--第二步主题目录下/next/_config.yml
search:
  path: search.xml
  field: post
  content: true
  format: html
local_search:
  enable: true
  # If auto, trigger search by changing input.
  # If manual, trigger search by pressing enter key or search button.
  trigger: auto
  # Show top n results per article, show all results by setting to -1
  top_n_per_article: 1
  # Unescape html strings to the readable one.
  unescape: false
  # Preload the search data when the page loads.
  preload: false
```

b.效果如下

![image-20210825193943934](g.png)

###### 2.风格设置

```shell
-- 个人最喜欢的风格，有四种风格可选
scheme: Pisces
```

###### 3.配置favicon

​	下载自己的icon图标，放在如下目录中，然后配置左边的图标路径即可

```shell
favicon:
  small: /images/gtx.png
  medium: /images/gtx.png
  apple_touch_icon: /images/gtx.png
  safari_pinned_tab: /images/gtx.png
```

![image-20210825200701077](h.png)

###### 4.配置分类tag

```
--站点目录下执行
hexo new page tags
```

然后在source目录中会生成一个tag包，tag包中有个文件，修改文件内容

```txt
---
title: tags
date: 2021-08-25 20:17:55
type: tags
---
```

然后在你的博文中添加如下内容即可

```
---
title: Hello World --博文标题
tags: [学习] --需要新增的内容，所属的tag
---
```

###### 5.配置头像

```shell
avatar:
  # Replace the default image and set the url here.
  url: /images/avatar.gif
  # If true, the avatar will be dispalyed in circle.
  rounded: true
  # If true, the avatar will be rotated with the cursor.
  rotated: true
```

###### 6.发布字数

```shell
-- 第一步执行
npm install hexo-word-counter 
hexo clean
--第二步配置
symbols_count_time:
  separated_meta: true
```

###### 7.热门推荐

```shell
--第一步执行
npm install hexo-related-popular-posts 
hexo clean
--第二步配置
related_posts:
  enable: true

```

