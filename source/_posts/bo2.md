---
title: 美化博客站点2-配置统计和及时聊天功能
tags: [博客]
---
### 						美化博客站点2-配置统计和及时聊天功能
<!-- more -->
##### 1.准备阶段

###### 1.1统计功能准备

​	a.准备统计功能服务，首先注册一个leanCloud账号，地址https://console.leancloud.app/apps

​	b.创建一个应用

​	![image-20210825190421994](a.png)

c.创建一个名称为Counter的class

![image-20210825190600304](b.png)

d.打开应用配置，查看对应的秘钥appId

![image-20210825190730497](c.png)

###### 1.2配置实时聊天功能

​	a.我们使用tidio工具实现免费在线聊天系统，注册账号地址https://www.tidio.com/ ,选择账号类型时，选择最左边的免费版本

​	b.获取publicKey

![image-20210825192948931](e.png)

##### 2.配置阶段

###### 2.1配置统计功能

​	a.安装包插件

```shell
-- 在站点目录下执行
 npm install hexo-leancloud-counter-security
```

​	b.配置站点目录下的_config.yml文件

```shell
--文件位置 hexo/_config.yml 
leancloud_counter_security:
  enable_sync: true
  app_id: <your app id> 上面注册的应用appId
  app_key: <your app key> 上面注册的应该秘钥
  username: <your username> # Will be asked while deploying if is left blank
  password: <your password> # Recommmended to be left blank. Will be asked while deploying if is 
```

​	c.配置next下的_config.yml文件

```
-- 主题目录 next/_config.yml
# Show number of visitors to each article.
# You can visit https://www.leancloud.cn get AppID and AppKey.
leancloud_visitors:
  enable: true
  app_id: #<app_id>
  app_key: #<app_key>
  # Required for apps from CN region
  server_url: # <your server url>
  # Dependencies: https://github.com/theme-next/hexo-leancloud-counter-security
  # If you don't care about security in lc counter and just want to use it directly
  # (without hexo-leancloud-counter-security plugin), set the `security` to `false`.
  security: true
```

​	d.上述配置完成，发布项目

​	hexo d -g

​	hexo server

​	效果如下

![image-20210825191227645](d.png)

###### 2.2使用在线聊天功能

​	a.配置上面获取的publicKey

```shell
-- 主题next目录下的_config.yml文件
tidio:
  enable: true
  key: 自己的秘钥
  
chat:
  enable: true
  service: tidio
  icon: fa fa-comment
  text: Chat
  
```

配置完成，查看效果

![image-20210825193214828](f.png)

以上就是我们新增的全部内容了，你学废了吗？

